#include "Library.h"

Library *Library::instance = nullptr;
Library::~Library() {
  while (!allFilms->empty()) {
    auto it = allFilms->begin();
    allFilms->erase(it);
    delete *it;
  }
  delete allFilms;
  instance = nullptr;
}

bool Library::isInLibraryByTitle(std::string title) {
  for (auto &it: *allFilms) {
    if (it->getTitle() == title) {
      return true;
    }
  }
  return false;
}

Film *Library::queryByTitle(std::string title) {
  Film *film = nullptr;
  for (auto &it: *allFilms) {
    if ( it->getTitle() == title ) {
      film = it;
      break;
    }
  }

  return film;
}

std::set<Film *, cmp> *Library::queryByGenre(std::string genre) {
  std::set<Film *, cmp> *result = new std::set<Film *, cmp>;
  for (auto &it: *allFilms) {
    if (it->getGenres().count(genre) == 1) {
      result->insert(it);
    }
  }
  return result;
}

void Library::removeByTitle(std::string title) {
  allFilms->erase(allFilms->find(queryByTitle(title)));
}
