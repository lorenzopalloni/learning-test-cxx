#ifndef LIBRARY_H
#define LIBRARY_H

#include <set>
#include <string>
#include "Film.h"

class Library {
private:
  static Library *instance;
  std::set<Film *, cmp> *allFilms;
  Library() { allFilms = new std::set<Film *, cmp>; }
public:
  ~Library();
  static Library *makeLibrary() {
    if (!instance) { instance = new Library; }
    return instance;
  }
  void addFilm(Film *film) { allFilms->insert(film); }
  std::set<Film *, cmp> *getAllFilms() { return allFilms; }
  bool isInLibrary(Film *film) { return (allFilms->count(film) == 1) ? true : false; }
  bool isInLibraryByTitle(std::string title);
  Film *queryByTitle(std::string);
  std::set<Film *, cmp> *queryByGenre(std::string genre);
  void removeByTitle(std::string title);
};

#endif
