#include <gtest/gtest.h>

#include <iostream>
#include <set>
#include <string>
#include <sstream>

#include "../Film.h"
#include "../Library.h"
#include "../Collection.h"
#include "../User.h"

template <typename T>
std::string captureCout(T &a) {
  std::stringstream buffer;
  std::streambuf *old = std::cout.rdbuf(buffer.rdbuf());
  std::cout << a << "\n";
  std::string result = buffer.str();
  std::cout.rdbuf(old);
  return result;
}

Film *buildFilm(std::string title = "hola", std::string year = "2009",
    std::string director = "Nolan", bool downloaded = true,
    std::set<std::string> genres = std::set<std::string>{"horror", "thriller"}) {
  return new Film(title, year, director, downloaded, genres);
}

Library *buildLibrary() {
  Library *lib = Library::makeLibrary();
  lib->addFilm(buildFilm());
  return lib;
}

Collection *buildCollection() {
  Collection *col = new Collection("holaCollection");
  col->addFilm(buildFilm());
  return col;
}

Collection *buildAnotherCollection() {
  Collection *col = new Collection("fooCollection");
  std::set<std::string> genresAnotherFilm{"horror", "adventure"};
  col->addFilm(buildFilm());
  col->addFilm(new Film("foo", "2009", "Nolan", true, genresAnotherFilm));
  return col;
}

TEST(testFilm, testGetters) {
  Film *film = buildFilm();
  ASSERT_TRUE(film->getTitle() == "hola");
  ASSERT_TRUE(film->getYear() == "2009");
  ASSERT_TRUE(film->getDirector() == "Nolan");
  ASSERT_TRUE(film->isDownloaded());
  ASSERT_TRUE(film->getGenres().count("horror") == 1);
  ASSERT_TRUE(film->getGenres().count("thriller") == 1);
  ASSERT_TRUE(film->getGenres().count("hola") == 0);
  delete film;
}

TEST(testFilm, testAddGenre) {
  Film *film = buildFilm();
  film->addGenre("adventure");
  ASSERT_TRUE(film->getGenres().count("adventure") == 1);
  delete film;
}

TEST(testLibrary, testSingleton) {
  Library *lib = Library::makeLibrary();
  Library *lib2 = Library::makeLibrary();
  ASSERT_EQ(lib, lib2);
  delete lib;
  lib2 = nullptr;
}

TEST(testLibrary, testGetAllFilms) {
  Film *film = buildFilm();
  Film *film2 = buildFilm("foo");
  Library *lib = Library::makeLibrary();
  std::set<Film *, cmp> *allFilms = lib->getAllFilms();
  allFilms->insert(film);
  allFilms->insert(film2);
  std::string expected = "< Title: hola, Year: 2009, Director: Nolan,\n"
                         "Downloaded: yes, Genres: horror thriller >\n";
  ASSERT_EQ(0, expected.compare(captureCout(*film)));
  delete lib;
//  while(!allFilms->empty()) {
//    auto it = allFilms->begin();
//    allFilms->erase(it);
//    delete *it;
//  }
}

TEST(testLibrary, testIsInLibrary) {
  Film *film = buildFilm();
  Film *film2 = buildFilm("2");
  Film *film3 = buildFilm("3");
  Library *lib = Library::makeLibrary();
  lib->addFilm(film);
  ASSERT_TRUE(lib->isInLibrary(film));
  ASSERT_TRUE(lib->isInLibrary(film2) == false);
  ASSERT_TRUE(lib->isInLibrary(film3) == false);
  delete film2, film3;
  delete lib;
}

TEST(testLibrary, testAddFilm) {
  Film *film = buildFilm();
  Library *lib = Library::makeLibrary();
  ASSERT_TRUE(lib->isInLibrary(film) == false);
  delete film;
  delete lib;
}

TEST(testLibrary, testIsInLibraryByTitle) {
  Library *lib = Library::makeLibrary();
  lib->addFilm(buildFilm("hola"));
  lib->addFilm(buildFilm("buba"));
  ASSERT_TRUE(lib->isInLibraryByTitle("buba"));
  ASSERT_TRUE(lib->isInLibraryByTitle("foo") == false);
  delete lib;
}

TEST(testLibrary, testQueryByTitle) {
  Library *lib = buildLibrary();
  Film *film = buildFilm();
  Film *film_queried = lib->queryByTitle("hola"); 
  ASSERT_TRUE(*film == *film_queried);
  delete lib;
  film_queried = nullptr;
}

TEST(testLibrary, testQueryByGenre) {
  Library *lib = buildLibrary();
  Film *film = buildFilm();
  std::set<Film *, cmp> *films_expected = new std::set<Film*, cmp>{film};
  std::set<Film *, cmp> *films_queried = lib->queryByGenre("horror");
  ASSERT_TRUE(films_expected->size() == films_queried->size());
  delete film;
  delete films_queried, films_expected;
  delete lib;
}

TEST(testLibrary, testRemoveByTitle) {
  Library *lib = buildLibrary();
  Film *film = buildFilm();
  lib->addFilm(film);
  ASSERT_TRUE(lib->isInLibrary(film));
  ASSERT_TRUE(lib->isInLibraryByTitle("hola"));
  lib->removeByTitle("hola");
  ASSERT_TRUE(!lib->isInLibrary(film));
}

TEST(testCollection, testAddFilm) {
  std::string name = "hola";
  Collection *col = new Collection(name);
  Film *film = buildFilm();
  col->addFilm(film);
  std::set<Film *, cmp> *copy_films = col->getFilms();
  ASSERT_TRUE(copy_films->count(film) == 1);
  delete col;
  film = nullptr;
  copy_films = nullptr;
}

TEST(tesCollection, testIsInByTitle) {
  Collection *col = buildCollection();
  ASSERT_TRUE(col->isInByTitle("hola"));
  delete col;
}

TEST(testCollection, testQueryByTitle) {
  Collection *col = buildCollection();
  Film *film = buildFilm();
  ASSERT_TRUE(*col->queryByTitle("hola") == *film);
  delete film;
  delete col;
}

TEST(testCollection, testQueryByGenre) {
  Collection *col = buildCollection();
  std::set<std::string> genresAnotherFilm{"horror", "adventure"};
  Film *anotherFilm = new Film("foo", "2009", "Nolan", true, genresAnotherFilm);
  col->addFilm(anotherFilm);
  std::set<Film *, cmp> *filmsAdventure = col->queryByGenre("adventure");
  ASSERT_TRUE(filmsAdventure->count(anotherFilm) == 1);
  delete filmsAdventure;
  delete col;
  anotherFilm = nullptr;
}

TEST(testUser, testAddCollection){
  Collection *col1 = buildCollection();
  Collection *col2 = buildAnotherCollection();
  User *user = new User("pallo");
  user->addCollection(col1);
  user->addCollection(col2);
  delete user;
  col2 = nullptr;
  col1 = nullptr;
}

TEST(testUser, testQueryDownloadedFilmByGenre) {
  Collection *col1 = buildCollection();
  Collection *col2 = buildAnotherCollection();
  User *user = new User("bigBubble");
  user->addCollection(col1);
  user->addCollection(col2);
  std::set<Film *, cmp> *adventureFilms = user->queryDownloadedFilmByGenre("adventure");
  std::set<std::string> genresAnotherFilm{"horror", "adventure"};
  Film *anotherFilm = new Film("foo", "2009", "Nolan", true, genresAnotherFilm); 
  ASSERT_TRUE(adventureFilms->count(anotherFilm) == 1);
  delete anotherFilm;
  delete adventureFilms;
  delete user;
  col2 = nullptr;
  col1 = nullptr;
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
