#include "Collection.h"

bool Collection::isInByTitle(std::string title) {
  for (auto &it: *m_films) {
    if (it->getTitle() == title) {
      return true;
    }
  }
  return false;
}

Film *Collection::queryByTitle(std::string title) {
  Film *film = nullptr;
  for (auto &it: *m_films) {
    if ( it->getTitle() == title ) {
      film = it;
      break;
    }
  }
  return film;
}

void Collection::removeByTitle(std::string title) {
  m_films->erase(m_films->find(queryByTitle(title)));
}

std::set<Film *, cmp> *Collection::queryByGenre(std::string genre) {
  std::set<Film *, cmp> *result = new std::set<Film *, cmp>;
  for (auto *it: *m_films) {
    if (it->getGenres().count(genre) == 1) {
      result->insert(it);
    }
  }
  return result;
}
