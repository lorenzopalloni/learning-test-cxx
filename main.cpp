#include <iostream>
#include "Film.h"

int main() {
  std::set<std::string> genres{"horror", "thriller"};
  Film *film = new Film("hola", "2009", "Nolan", true, genres);

  std::cout << *film << std::endl;

  return 0;
}
