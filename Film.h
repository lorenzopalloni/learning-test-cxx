#ifndef FILM_H
#define FILM_H

#include <iostream>
#include <string>
#include <set>

class Film {
private:
  std::string m_title;
  std::string m_year;
  std::string m_director;
  bool m_downloaded;
  std::set<std::string> m_genres;
public:
  Film(std::string title, std::string year, std::string director, bool downloaded,
      std::set<std::string> genres);

  std::string getTitle() { return m_title; }
  std::string getTitle() const { return m_title; }
  std::string getYear() { return m_year; }
  std::string getDirector() { return m_director; }
  bool isDownloaded() { return m_downloaded; }
  std::set<std::string> getGenres() { return m_genres; }
  std::set<std::string> getGenres() const { return m_genres; }

  void addGenre(std::string genre) { m_genres.insert(genre); }
  bool operator== (Film &rhs) {
    if (m_title != rhs.getTitle()) { return false; }
    if (m_year != rhs.getYear()) { return false; }
    if (m_director != rhs.getDirector()) { return false; }
    if (m_downloaded != rhs.isDownloaded()) { return false; }
    if (m_genres != rhs.getGenres()) { return false; }
    return true;
  }

  friend std::ostream& operator<< (std::ostream &out, Film& film);
};

struct cmp {
  bool operator() (const Film *lhs, const Film *rhs) const {
    return lhs->getTitle() < rhs->getTitle();
  }
};

#endif
