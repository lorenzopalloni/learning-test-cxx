#set -x

if [ "$1" == "-b" ]; then
  g++ -std=c++11 ./main.cpp Film.cpp Library.cpp -o main
  echo ">> Original version compiled.";
else
  echo ">> Warning: compiling only test version.";
  echo ">> Adding -b to compile both original and test versions.";
fi
g++ -std=c++11 ./tests/main.cpp Film.cpp Library.cpp Collection.cpp User.cpp -lgtest -o ./tests/main
if [ $? -eq 0 ]; then
  echo ">> Test version compiled.";
else
  echo ">> Error!! :C.";
  exit 1;
fi
exit 0
