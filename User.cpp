#include "User.h"

User::~User() {
  while (!m_collections->empty()) {
    auto it = m_collections->begin();
    m_collections->erase(it);
    delete *it;
  }
  if (m_collections != nullptr) {
    delete m_collections;
  }
}

std::set<Film *, cmp> *User::queryDownloadedFilmByGenre(std::string genre) {
  std::set<Film *, cmp> *result = new std::set<Film *, cmp>;
  for (auto it = m_collections->begin(); it != m_collections->end(); ++it) {
    std::set<Film *, cmp> *filmsByGenre = (*it)->queryByGenre(genre);
    for (auto itNested = filmsByGenre->begin();
              itNested != filmsByGenre->end(); ++itNested) {
      if ((*itNested)->isDownloaded()) {
        result->insert(*itNested);
      }
    }
  }
  return result;
}

